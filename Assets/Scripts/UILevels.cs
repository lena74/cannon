﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UILevels : MonoBehaviour
{
    [SerializeField] private Cannon _cannon;

    [Header("Panels")]
    [SerializeField] private GameObject _nextLevelPanel;
    [SerializeField] private GameObject _restartLevelPanel;
    [SerializeField] private GameObject _winPanel;

    [Header("Text")]
    [SerializeField] private TextMeshProUGUI _currentLevelText;
    [SerializeField] private TextMeshProUGUI _nextLevelText;
    [SerializeField] private TextMeshProUGUI _shootCountText;

    [Header("Images")]
    [SerializeField] private List<Image> _levelButtons;
    [SerializeField] private Sprite _darkSprite;
    [SerializeField] private Sprite _normalSprite;
    [SerializeField] private Sprite _activeSprite;

    [Header("Buttons")]
    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _nextButton;
    [SerializeField] private Button _exitButton;

    void Awake()
    {
        _nextLevelPanel.SetActive(false);
        _restartLevelPanel.SetActive(false);
        _winPanel.SetActive(false);

        _restartButton.onClick.AddListener(() => 
        {
            _cannon.StartLevel(false);
            _restartLevelPanel.SetActive(false);
        });

        _nextButton.onClick.AddListener(() =>
        {
            _cannon.StartLevel(true);
            _nextLevelPanel.SetActive(false);
            RedrawLevelsLine();
        });

        RedrawLevelsLine();
    }

    void Update()
    {
        _shootCountText.text = _cannon.ShootCount.ToString();

        if (!_cannon.Win)
        {
            _restartLevelPanel.SetActive(_cannon.Failed);
            _nextLevelPanel.SetActive(_cannon.Success);
        }
        else _winPanel.SetActive(true);
    }

    private void RedrawLevelsLine()
    {
        _currentLevelText.text = _cannon.CurrentLevel.ToString();

        if (_cannon.CurrentLevel != _cannon.LevelsCount)
        {
            _nextLevelText.text = (_cannon.CurrentLevel + 1).ToString();
        }
        else _nextLevelText.text = "";

        for (int i = 0; i < _cannon.CurrentLevel - 1; i++)
        {
            _levelButtons[i].sprite = _normalSprite;
        }

        _levelButtons[_cannon.CurrentLevel - 1].sprite = _activeSprite;

        for (int i = _cannon.CurrentLevel; i < _levelButtons.Count; i++)
        {
            _levelButtons[i].sprite = _darkSprite;
        }
    }
}
