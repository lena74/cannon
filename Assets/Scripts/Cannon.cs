﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public int CurrentLevel
    {
        get { return currentLevel; }
        set { currentLevel = value; }
    }
    public int LevelsCount => _levelPrefabs.Count;
    public int ShootCount => shootCount - shootRealized;
    public bool Success => success;
    public bool Failed => failed;
    public bool Win => win;

    [SerializeField] private Rigidbody _projectile;
    [SerializeField] private Transform _cannonTransform;
    [SerializeField] private Transform _cannonLookAtPoint;
    [SerializeField] private List<GameObject> _levelPrefabs;

    private RaycastHit hit;
    private GameObject level;
    private int currentLevel;
    private int shootCount;
    private int shootRealized;
    private bool failed;
    private bool success;
    private bool win;
    private bool paused;

    void Awake()
    {
        hit = new RaycastHit();

        currentLevel = 1;
        shootRealized = 0;
        shootCount = currentLevel * 3;

        level = Instantiate(_levelPrefabs[0]);
    }

    void Update()
    {
        if (!paused)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    if (Physics.Raycast(ray, out hit))
                    {
                        Vector3 relativePos = hit.point - _cannonTransform.position;
                        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
                        _cannonTransform.rotation = new Quaternion(Mathf.Clamp(rotation.x, -0.1f, 0.1f), rotation.y, rotation.z, rotation.w);

                        if (shootCount > shootRealized)
                        {
                            Rigidbody rb = Instantiate(_projectile, _cannonLookAtPoint.position, _cannonLookAtPoint.rotation);
                            rb.AddForce(_cannonLookAtPoint.up * 7000f);
                            Destroy(rb.gameObject, 2f);
                            shootRealized++;
                            if (shootCount - shootRealized == 0)
                            {
                                StartCoroutine(CheckFailed());
                            }
                        }
                    }
                }
            }
        }
    }

    public void StartLevel(bool next)
    {
        Destroy(level);
        success = failed = paused = false;

        if (next)
        {
            level = Instantiate(_levelPrefabs[currentLevel]);
            currentLevel++;
        }
        else level = Instantiate(_levelPrefabs[currentLevel - 1]);

        shootCount = currentLevel * 3;
        shootRealized = 0;
    }

    private void OnCollisionEnter(Collision collision)
    {
        int counter = level.transform.childCount;
        if (counter == 2)
        {
            if (currentLevel == _levelPrefabs.Count)
            {
                win = paused = true;
            }
            else
            {
                StopAllCoroutines();
                success = paused = true;
                FacebookManager.Instance.LevelCompleted(currentLevel);
            }
        }
        Destroy(collision.gameObject);
    }

    IEnumerator CheckFailed()
    {
        yield return new WaitForSeconds(5f);
        failed = true;
    }
}
